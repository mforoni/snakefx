package com.github.mforoni.snakefx;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.LinkedList;
import java.util.List;

public final class Snake {

  public static final Color COLOR = Color.CORNSILK;
  public static final Color DEAD = Color.RED;
  private final Grid grid;
  private final List<Point> points = new LinkedList<>();
  private Velocity velocity;
  private boolean alive;

  /**
   * Constructs a new {@code Snake}. It takes the {@link Grid} where this {@code Snake} can move and
   * the initial point for the head. The snake's velocity is initialized to {@link Velocity#STILL}.
   *
   * @param grid The {@code Grid} where this {@code Snake} can move
   * @param initial The initial {@code Point} of the snake's head
   * @see Grid
   * @see Point
   */
  public Snake(final Grid grid, final Point initial) {
    this.grid = grid;
    points.add(initial);
    alive = true;
    velocity = Velocity.ONE_PIXEL_RIGHT;
  }

  public void reset() {
    points.clear();
    final Point p = Points.random(grid.getRows(), grid.getCols());
    points.add(p);
    velocity = Velocity.ONE_PIXEL_RIGHT;
    alive = true;
  }

  public int getLength() {
    return points.size();
  }

  /**
   * @return The points occupied by the snake.
   */
  public List<Point> getPoints() {
    return points;
  }

  /**
   * Checks for an intersection and marks the "alive" flag accordingly.
   *
   * @param point The new Point to move to.
   */
  private void checkAndAdd(Point point) {
    alive &= !points.contains(point);
    points.add(point);
  }

  /**
   * @return {@code true} if the Snake hasn't run into itself yet.
   */
  public boolean isAlive() {
    return alive;
  }

  /**
   * @return The location of the head of the {@code Snake}.
   */
  public Point getHead() {
    return points.get(getLength() - 1);
  }

  /**
   * Make the snake move one square in it's current direction.
   */
  public void move() {
    if (isAlive()) {
      // The head goes to the new location
      checkAndAdd(nextPoint());
      // The last/oldest position is dropped
      points.remove(0);
    }
  }

  public Point nextPoint() {
    final Point point = Points.translate(getHead(), velocity);
    return grid.wrap(point);
  }

  /**
   * Make the snake extend/grow to the square where it's headed.
   * It increases the length of the snake by one.
   */
  public void moveAndEat() {
    if (isAlive()) {
      checkAndAdd(nextPoint());
    }
  }

  public void setUp() {
    if (velocity != Velocity.ONE_PIXEL_DOWN) {
      velocity = Velocity.ONE_PIXEL_UP;
    }
  }

  public void setDown() {
    if (velocity != Velocity.ONE_PIXEL_UP) {
      velocity = Velocity.ONE_PIXEL_DOWN;
    }
  }

  public void setLeft() {
    if (velocity != Velocity.ONE_PIXEL_RIGHT) {
      velocity = Velocity.ONE_PIXEL_LEFT;
    }
  }

  public void setRight() {
    if (velocity != Velocity.ONE_PIXEL_LEFT) {
      velocity = Velocity.ONE_PIXEL_RIGHT;
    }
  }

  public void paint(final GraphicsContext gc) {
    gc.setFill(Snake.COLOR);
    final int size = grid.getSize();
    points.forEach(p -> p.paint(gc, size));
    if (!isAlive()) {
      gc.setFill(Snake.DEAD);
      getHead().paint(gc, size);
    }
  }

  static class Velocity {

    private static final Velocity STILL = new Velocity(0, 0);
    public static final Velocity ONE_PIXEL_LEFT = new Velocity(-1, 0);
    public static final Velocity ONE_PIXEL_RIGHT = new Velocity(1, 0);
    public static final Velocity ONE_PIXEL_DOWN = new Velocity(0, 1);
    public static final Velocity ONE_PIXEL_UP = new Velocity(0, -1);
    private final int dx;
    private final int dy;

    Velocity(int dx, int y) {
      this.dx = dx;
      this.dy = y;
    }

    public int getDx() {
      return dx;
    }

    public int getDy() {
      return dy;
    }
  }
}
