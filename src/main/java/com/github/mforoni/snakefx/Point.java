package com.github.mforoni.snakefx;

import java.util.Objects;
import javafx.scene.canvas.GraphicsContext;

/**
 * A point of a Cartesian coordinate system.
 *
 * @author Foroni Marco
 */
public final class Point {

  private final int x;
  private final int y;

  /**
   * Constructs a point with integer coordinate in a two-dimensional space.
   *
   * @param x The X coordinate.
   * @param y The Y coordinate.
   */
  public Point(final int x, final int y) {
    this.x = x;
    this.y = y;
  }

  /**
   * The X coordinate.
   *
   * @return The X coordinate.
   */
  public int getX() {
    return x;
  }

  /**
   * Returns the y coordinate.
   *
   * @return The Y coordinate.
   */
  public int getY() {
    return y;
  }

  /**
   * Paints on the given {@link GraphicsContext} a square having side length equals to the specified
   * {@code size}.
   *
   * @see GraphicsContext#fillRect(double, double, double, double)
   */
  public void paint(final GraphicsContext gc, final int size) {
    gc.fillRect(x * size, y * size, size, size);
  }

  /**
   * @param other The "other" point to compare against.
   * @return {@code true} if the other {@code Object} is an instance of Point and has the same
   * coordinates.
   */
  @Override
  public boolean equals(Object other) {
    if (!(other instanceof Point)) {
      return false;
    }
    Point point = (Point) other;
    return x == point.x && y == point.y;
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y);
  }

  @Override
  public String toString() {
    return x + ", " + y;
  }
}