package com.github.mforoni.snakefx;

import com.github.mforoni.snakefx.SnakeGame.UserInput;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

public class SnakeEventHandler implements EventHandler<KeyEvent> {

  private final SnakeGame snakeGame;

  public SnakeEventHandler(final SnakeGame snakeGame) {
    this.snakeGame = snakeGame;
  }

  @Override
  public void handle(KeyEvent e) {
    switch (e.getCode()) {
      case P:
        snakeGame.pauseResume();
        break;
      case UP:
        snakeGame.setUserInput(UserInput.UP);
        break;
      case DOWN:
        snakeGame.setUserInput(UserInput.DOWN);
        break;
      case LEFT:
        snakeGame.setUserInput(UserInput.LEFT);
        break;
      case RIGHT:
        snakeGame.setUserInput(UserInput.RIGHT);
        break;
      case ENTER:
        switch (snakeGame.getState()) {
          case LOADING:
            snakeGame.start();
            break;
          case GAME_OVER:
            final Snake snake = snakeGame.getGrid().getSnake();
            snake.reset();
            snakeGame.start();
            break;
          default:
            // do nothing
        }
    }
  }
}
