package com.github.mforoni.snakefx;

import com.github.mforoni.snakefx.SnakeGame.UserInput;
import com.google.common.collect.ImmutableSet;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * The positional system for the game. This grid will be rendered in the Canvas.
 */
public final class Grid {

  public static final Color COLOR = new Color(0.1, 0.1, 0.1, 1);
  private final int cols;
  private final int rows;
  private final int size;
  private final double width;
  private final double height;
  private final Snake snake;
  private Point food;

  /**
   * Constructs a {@code Grid} having the specified {@code rows} and {@code columns}, and the side
   * length of each square point in the grid is equals to the given {@code size}.
   */
  public Grid(final int rows, final int cols, final int size) {
    this.rows = rows;
    this.cols = cols;
    this.size = size;
    width = rows * size;
    height = cols * size;
    // initialize the snake at the centre of the screen
    snake = new Snake(this, new Point(rows / 2, cols / 2));
    // put the food at a random location
    food = Points.random(rows, cols, ImmutableSet.copyOf(snake.getPoints()));
  }

  public int getCols() {
    return cols;
  }

  public int getRows() {
    return rows;
  }

  public int getSize() {
    return size;
  }

  public double getWidth() {
    return width;
  }

  public double getHeight() {
    return height;
  }

  public Snake getSnake() {
    return snake;
  }

  public Point getFood() {
    return food;
  }

  public Point wrap(final Point point) {
    int x = point.getX();
    int y = point.getY();
    if (x >= rows) {
      x = 0;
    }
    if (y >= cols) {
      y = 0;
    }
    if (x < 0) {
      x = rows - 1;
    }
    if (y < 0) {
      y = cols - 1;
    }
    return new Point(x, y);
  }

  /**
   * This method is called in every cycle of execution.
   */
  public void update(final UserInput userInput) {
    switch (userInput) {
      case UP:
        snake.setUp();
        break;
      case DOWN:
        snake.setDown();
        break;
      case LEFT:
        snake.setLeft();
        break;
      case RIGHT:
        snake.setRight();;
        break;
    }
    if (food.equals(snake.nextPoint())) {
      snake.moveAndEat();
      food = Points.random(rows, cols, ImmutableSet.copyOf(snake.getPoints()));
      System.out.println("Generate food at " + food);
    } else {
      snake.move();
    }
  }

  public void paint(final GraphicsContext gc) {
    gc.setFill(Color.BLACK);
    gc.fillRect(0, 0, getWidth(), getHeight());
    // Now the Food
    gc.setFill(Color.ROSYBROWN);
    food.paint(gc, size);
    // Now the snake
    snake.paint(gc);
    // The score
    gc.setFill(Color.BEIGE);
    gc.fillText("Score : " + 100 * snake.getPoints().size(), 10, getHeight() - 10);
  }
}
