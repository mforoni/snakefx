package com.github.mforoni.snakefx;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

final class SnakeGame {

  public enum State {
    LOADING, PAUSED, IN_GAME, GAME_OVER
  }

  public enum UserInput {
    NONE, UP, DOWN, LEFT, RIGHT
  }

  private final Grid grid;
  private final GameLoop gameLoop;
  private final Thread thread;
  private UserInput userInput;

  SnakeGame(final Grid grid, final GraphicsContext gc) {
    this.grid = grid;
    gameLoop = new GameLoop(grid, gc, 10);
    thread = new Thread(gameLoop);
    userInput = UserInput.NONE;
  }

  public Grid getGrid() {
    return grid;
  }

  public UserInput getUserInput() {
    return userInput;
  }

  public void setUserInput(UserInput userInput) {
    this.userInput = userInput;
  }

  public void run() {
    thread.start();
  }

  public State getState() {
    return gameLoop.state;
  }

  public synchronized void pauseResume() {
    if (gameLoop.state == State.PAUSED) {
      gameLoop.state = State.IN_GAME;
    } else if (gameLoop.state == State.IN_GAME) {
      gameLoop.state = State.PAUSED;
    }
  }

  public synchronized void start() {
    gameLoop.state = State.IN_GAME;
  }

  private class GameLoop implements Runnable {

    private static final int DEFAULT_SLEEP_TIME = 250;
    private static final int DEFAULT_FRAME_RATE = 25; //30;
    private final Grid grid;
    private final GraphicsContext gc;
    private final float interval;
    private State state;


    public GameLoop(final Grid grid, final GraphicsContext gc, final int frameRate) {
      this.grid = grid;
      this.gc = gc;
      interval = 1000.0f / frameRate;
      state = State.LOADING;
    }

    public GameLoop(final Grid grid, final GraphicsContext context) {
      this(grid, context, DEFAULT_FRAME_RATE);
    }

    @Override
    public void run() {
      grid.paint(gc);
      while (true) {
        switch (state) {
          case LOADING:
            messageCycle("Welcome to SnakeFX! Hit RETURN to start the game.", DEFAULT_SLEEP_TIME);
            break;
          case PAUSED:
//            System.out.println("snake head = " + grid.getSnake().getHead());
//            System.out.println("food = " + grid.getFood());
            messageCycle("Game paused. Press P to resume...", DEFAULT_SLEEP_TIME);
            break;
          case IN_GAME:
            gameCycle();
            break;
          case GAME_OVER:
            messageCycle("Game Over! Hit RETURN to restart the game.", Color.RED,
                DEFAULT_SLEEP_TIME);
            break;
          default:
            throw new AssertionError();
        }
      }
    }

    private void messageCycle(final String msg, final int sleepTime) {
      messageCycle(msg, Color.AQUA, sleepTime);
    }

    private void messageCycle(final String msg, final Color color, final int sleepTime) {
      paintMessage(gc, color, msg);
      try {
        Thread.sleep(sleepTime);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    private void gameCycle() {
      // Time the update and paint calls
      float time = System.currentTimeMillis();
      grid.update(userInput);
      userInput = UserInput.NONE;
      grid.paint(gc);
      time = System.currentTimeMillis() - time;
      // Adjust the timing correctly
      if (time < interval) {
        try {
          Thread.sleep((long) (interval - time));
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      if (!grid.getSnake().isAlive()) {
        state = State.GAME_OVER;
      }
    }
  }

  public static void paintMessage(final GraphicsContext gc, final String msg) {
    paintMessage(gc, Color.AQUA, msg);
  }

  public static void paintMessage(final GraphicsContext gc, final Color color, final String msg) {
    gc.setFill(color);
    gc.fillText(msg, 10, 10);
  }

}
