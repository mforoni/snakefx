package com.github.mforoni.snakefx;


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


public final class SnakeApp extends Application {

  private static final int ROWS = 35; //50;
  private static final int COLUMNS = 35; //50;
  public static final int DEFAULT_SIZE = 10;

  public static void main(String[] args) {
    Application.launch(args);
  }

  @Override
  public void start(final Stage primaryStage) {
    final Grid grid = new Grid(ROWS, COLUMNS, DEFAULT_SIZE);
    final Canvas canvas = new Canvas(grid.getWidth(), grid.getHeight());
    final GraphicsContext context = canvas.getGraphicsContext2D();
    final SnakeGame snakeGame = new SnakeGame(grid, context);
    canvas.setFocusTraversable(true);
    canvas.setOnKeyPressed(new SnakeEventHandler(snakeGame));
    final StackPane root = new StackPane();
    root.getChildren().add(canvas);
    primaryStage.setResizable(false);
    primaryStage.setTitle("SnakeFX");
    primaryStage.setOnCloseRequest(e -> System.exit(0));
    final Scene scene = new Scene(root);
    primaryStage.setScene(scene);
    primaryStage.show();
    snakeGame.run();
  }
}