package com.github.mforoni.snakefx;

import com.github.mforoni.snakefx.Snake.Velocity;
import com.google.common.annotations.Beta;

import java.util.Random;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Provides {@code static} utilities methods for manipulating {@code Point} instances.
 *
 * @author Foroni Marco
 */
public final class Points {

  private Points() {
    throw new AssertionError();
  }

  /**
   * Returns a new {@code Point} having random positive coordinates in a two-dimensional space,
   * having {@code x} coordinate not greater than {@code maxX} and {@code y} coordinate not greater
   * than {@code maxY}.
   *
   * @param maxX The maximum value for the {@code x} coordinate
   * @param maxY The maximum value for the {@code y} coordinate
   * @return A new random {@code Point}
   */
  @Nonnull
  public static Point random(final int maxX, final int maxY) {
    final Random random = new Random();
    return new Point(random.nextInt(maxX), random.nextInt(maxY));
  }

  /**
   * Returns a new {@code Point} having random positive coordinates in a two-dimensional space,
   * where the {@code x} coordinate is not greater than {@code maxX} and the {@code y} coordinate is
   * not greater than {@code maxY}. It also ensures that the generated random point is not included
   * in the {@code excluded} set.
   *
   * @param maxX The maximum value for the {@code x} coordinate
   * @param maxY The maximum value for the {@code y} coordinate
   * @param excluded A set of excluded {@code Point} objects that needs to be avoided
   * @return A new random {@code Point}
   */
  @Beta
  @Nullable
  public static Point random(final int maxX, final int maxY, @Nonnull final Set<Point> excluded) {
    final int domainSize = maxX * maxY;
    if (domainSize > excluded.size()) {
      final Random random = new Random();
      Point point;
      do {
        point = new Point(random.nextInt(maxX ), random.nextInt(maxY));
      } while (excluded.contains(point));
      return point;
    }
    return null;
  }

  /**
   * Returns a new {@code Point} which is the result of the translation of the specified {@code
   * point} with the given {@code velocity}.
   *
   * @return A new Point which is the result of translation of this point.
   */
  @Nonnull
  public static Point translate(final Point point, final Velocity velocity) {
    return new Point(point.getX() + velocity.getDx(), point.getY() + velocity.getDy());
  }
}
