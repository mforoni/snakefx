# SnakeFX

A simple Java clone of the famous *Snake* game made as an excuse to experiment with *JavaFX*. 

## History

The concept originated in the 1976 arcade game Blockade, and the ease of implementing Snake has led to hundreds of versions (some of which have the word snake or worm in the title) for many platforms.
Read more about snake history at [wikipedia](https://en.wikipedia.org/wiki/Snake_(video_game_genre)#History).

## Built With

* [Gradle](https://gradle.org/)

## Code Style

This project follow the [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html).

## Author

* **Marco Foroni** - [mforoni](https://gitlab.com/mforoni)

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/mforoni/snakefx/blob/master/LICENSE) file for details.